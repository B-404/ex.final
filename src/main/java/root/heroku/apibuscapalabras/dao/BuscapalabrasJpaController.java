/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.heroku.apibuscapalabras.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.heroku.apibuscapalabras.BuscaPalabras;
import root.heroku.apibuscapalabras.dao.exceptions.NonexistentEntityException;
import root.heroku.apibuscapalabras.dao.exceptions.PreexistingEntityException;
import root.heroku.apibuscapalabras.entity.Buscapalabras;

/**
 *
 * @author ELIAS
 */
public class BuscapalabrasJpaController implements Serializable {

    public BuscapalabrasJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("clientes_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Buscapalabras buscapalabras) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(buscapalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBuscapalabras(buscapalabras.getNombre()) != null) {
                throw new PreexistingEntityException("Buscapalabras " + buscapalabras + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Buscapalabras buscapalabras) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            buscapalabras = em.merge(buscapalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = buscapalabras.getNombre();
                if (findBuscapalabras(id) == null) {
                    throw new NonexistentEntityException("The buscapalabras with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Buscapalabras buscapalabras;
            try {
                buscapalabras = em.getReference(Buscapalabras.class, id);
                buscapalabras.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The buscapalabras with id " + id + " no longer exists.", enfe);
            }
            em.remove(buscapalabras);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Buscapalabras> findBuscapalabrasEntities() {
        return findBuscapalabrasEntities(true, -1, -1);
    }

    public List<Buscapalabras> findBuscapalabrasEntities(int maxResults, int firstResult) {
        return findBuscapalabrasEntities(false, maxResults, firstResult);
    }

    private List<Buscapalabras> findBuscapalabrasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Buscapalabras.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Buscapalabras findBuscapalabras(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Buscapalabras.class, id);
        } finally {
            em.close();
        }
    }

    public int getBuscapalabrasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Buscapalabras> rt = cq.from(Buscapalabras.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public void create(BuscaPalabras buscapalabras) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
