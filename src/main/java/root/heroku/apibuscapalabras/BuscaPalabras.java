
package root.heroku.apibuscapalabras;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.heroku.apibuscapalabras.dao.BuscapalabrasJpaController;
import root.heroku.apibuscapalabras.entity.Buscapalabras;

@Path("palabras")
public class BuscaPalabras {
    
   @GET
   @Produces(MediaType.APPLICATION_JSON)
   public Response listarBusqueda(){
    
   BuscapalabrasJpaController  dao=new  BuscapalabrasJpaController();
   List<Buscapalabras>lista=dao.findBuscapalabrasEntities();
   
   return Response.ok(200).entity(lista).build();
   
    
}    
   @POST
   @Produces(MediaType.APPLICATION_JSON)
   public Response agregar(){
   

      
      BuscapalabrasJpaController  dao=new  BuscapalabrasJpaController();
      Buscapalabras BuscaPalabras = null;
      try {
          dao.create(BuscaPalabras);
      } catch (Exception ex) {
          Logger.getLogger(BuscaPalabras.class.getName()).log(Level.SEVERE, null, ex);
      }
      return Response.ok(200).entity(BuscaPalabras).build();
  }
   
   }
    

