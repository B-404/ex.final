/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.heroku.apibuscapalabras.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ELIAS
 */
@Entity
@Table(name = "buscapalabras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Buscapalabras.findAll", query = "SELECT b FROM Buscapalabras b"),
    @NamedQuery(name = "Buscapalabras.findByNombre", query = "SELECT b FROM Buscapalabras b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "Buscapalabras.findBySignificado", query = "SELECT b FROM Buscapalabras b WHERE b.significado = :significado"),
    @NamedQuery(name = "Buscapalabras.findByFecha", query = "SELECT b FROM Buscapalabras b WHERE b.fecha = :fecha")})
public class Buscapalabras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "significado")
    private String significado;
    @Size(max = 2147483647)
    @Column(name = "fecha")
    private String fecha;

    public Buscapalabras() {
    }

    public Buscapalabras(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Buscapalabras)) {
            return false;
        }
        Buscapalabras other = (Buscapalabras) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.heroku.apibuscapalabras.entity.Buscapalabras[ nombre=" + nombre + " ]";
    }
    
}
